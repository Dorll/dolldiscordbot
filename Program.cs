﻿
using Discord;
using Discord.WebSocket;

namespace DorllBot
{
    public class Program
    {
        public static void Main(string[] args) => new Program().MainAsync().GetAwaiter().GetResult();

        // This is a test

        private DiscordSocketClient client = default(DiscordSocketClient);

        public async Task MainAsync()
        {
            client = new DiscordSocketClient();




            client.Log += Log;

            await LoadFile();

            var token = Environment.GetEnvironmentVariable("TOKEN");
            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();

            await Task.Delay(-1);

        }
        private Task LoadFile()
        {
            var root = Directory.GetCurrentDirectory();
            var dotenv = Path.Combine(root, ".env");
            DotEnv.Load(dotenv);
            return Task.CompletedTask;
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }
    }
}